﻿Pre typy Q som vytvorila jednu abstraktnu triedu a jej potomky s funkciou getbits, ktora vracia pocet bitov za ciarkou.
U Fixed<> som uprednostnila struct pred class, pretoze instancia obsahuje len jeden int, takze class by bol moc velky overhead. Naviac sa jedna o reprezentaciu cisla, takze dava vacsi zmysel predavanie hodnotou.
K praci s 32 bitovou presnostou som pouzila int, ktory ma prave 32 bitov.
Najtazsia cast bola ako od generickeho typu Q ziskat presnost, pretoze override na staticku metodu nefunguje. To som nakoniec vyriesila tym, ze vytvaram instanciu, na ktoru metodu zavolam.
Nejako sa mi pokazil git tak, ze zaznamenaval len jeden projekt z 3, tak som ho musela na konci opravovat.
