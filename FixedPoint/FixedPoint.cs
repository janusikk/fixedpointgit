﻿using System;

namespace Cuni.Arithmetics.FixedPoint
{
	public abstract class QType
	{
		public abstract int getBits();
	}
	public class Q8_24 : QType
	{
		public override int getBits() => 24;
	}
	public class Q16_16 : QType
	{
		public override int getBits() => 16;
	}
	public class Q24_8 : QType
	{
		public override int getBits() => 8;
	}
	public struct Fixed<Q> where Q : QType, new()
	{
		static int bits = (new Q()).getBits();
		private int value;
		public Fixed(int i)
		{
			value = i << bits;
		}
		public static Fixed<Q> operator +(Fixed<Q> a, Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			result.value = a.value + b.value;
			return result;
		}
		public Fixed<Q> Add(Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			result.value = this.value + b.value;
			return result;
		}
		public static Fixed<Q> operator -(Fixed<Q> a, Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			result.value = a.value - b.value;
			return result;
		}
		public Fixed<Q> Subtract(Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			result.value = this.value - b.value;
			return result;
		}
		public static Fixed<Q> operator *(Fixed<Q> a, Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			long x = (long)a.value * (long)b.value;
			result.value = (int)(x >> bits);
			return result;
		}
		public Fixed<Q> Multiply(Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			long x = (long)this.value * (long)b.value;
			result.value = (int)(x >> bits);
			return result;
		}
		public static Fixed<Q> operator /(Fixed<Q> a, Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			long x = ((long)a.value << bits) / (long)b.value;
			result.value = (int)x;
			return result;
		}
		public Fixed<Q> Divide(Fixed<Q> b)
		{
			var result = new Fixed<Q>();
			long x = ((long)this.value << bits) / (long)b.value;
			result.value = (int)x;
			return result;
		}
		public double ToDouble()
		{
			double x = (double)value / (double)(1 << bits);
			return x;
		}
		public override string ToString()
		{
			double d = ToDouble();
			return d.ToString();
		}
	}
}
