using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cuni.Arithmetics.FixedPoint;
namespace FixedPointUnitTests
{
    [TestClass]
    public class TestFixed8_24
    {
        [TestMethod]
        public void TestToString()
        {
            var a = new Fixed<Q8_24>(25);
            Assert.AreEqual("25", a.ToString());
        }
        [TestMethod]
        public void TestAdd()
        {
            var f1 = new Fixed<Q8_24>(3);
            var f2 = new Fixed<Q8_24>(2);
            Assert.AreEqual("5", f1.Add(f2).ToString());
        }
        [TestMethod]
        public void TestMultiply()
        {
            var f1 = new Fixed<Q8_24>(3);
            var f2 = new Fixed<Q8_24>(2);
            Assert.AreEqual("6", f1.Multiply(f2).ToString());
        }
        [TestMethod]
        public void TestOverFlow()
        {
            var f1 = new Fixed<Q8_24>(19);
            var f2 = new Fixed<Q8_24>(13);
            Assert.AreEqual("-9", f1.Multiply(f2).ToString());
        }
        [TestMethod]
        public void TestDivide1()
        {
            var f1 = new Fixed<Q8_24>(248);
            var f2 = new Fixed<Q8_24>(10);
            Assert.AreEqual("-0,799999952316284", f1.Divide(f2).ToString());
        }
        [TestMethod]
        public void TestDivide2()
        {
            var f1 = new Fixed<Q8_24>(625);
            var f2 = new Fixed<Q8_24>(1000);
            Assert.AreEqual("-4,70833331346512", f1.Divide(f2).ToString());
        }
    }
    [TestClass]
    public class TestFixed16_16
    {
        [TestMethod]
        public void TestToString()
        {
            var a = new Fixed<Q16_16>(3);
            Assert.AreEqual("3", a.ToString());
        }
        [TestMethod]
        public void TestAdd()
        {
            var f1 = new Fixed<Q16_16>(3);
            var f2 = new Fixed<Q16_16>(2);
            Assert.AreEqual("5", f1.Add(f2).ToString());
        }
        [TestMethod]
        public void TestMultiply()
        {
            var f1 = new Fixed<Q16_16>(3);
            var f2 = new Fixed<Q16_16>(2);
            Assert.AreEqual("6", f1.Multiply(f2).ToString());
        }
        [TestMethod]
        public void TestOverFlow()
        {
            var f1 = new Fixed<Q16_16>(19);
            var f2 = new Fixed<Q16_16>(13);
            Assert.AreEqual("247", f1.Multiply(f2).ToString());
        }
        [TestMethod]
        public void TestDivide1()
        {
            var f1 = new Fixed<Q16_16>(248);
            var f2 = new Fixed<Q16_16>(10);
            Assert.AreEqual("24,7999877929688", f1.Divide(f2).ToString());
        }
        [TestMethod]
        public void TestDivide2()
        {
            var f1 = new Fixed<Q16_16>(625);
            var f2 = new Fixed<Q16_16>(1000);
            Assert.AreEqual("0,625", f1.Divide(f2).ToString());
        }
    }
    [TestClass]
    public class TestFixed24_8
    {
        [TestMethod]
        public void TestToString()
        {
            var a = new Fixed<Q24_8>(3);
            Assert.AreEqual("3", a.ToString());
        }
        [TestMethod]
        public void TestAdd()
        {
            var f1 = new Fixed<Q24_8>(3);
            var f2 = new Fixed<Q24_8>(2);
            Assert.AreEqual("5", f1.Add(f2).ToString());
        }
        [TestMethod]
        public void TestMultiply()
        {
            var f1 = new Fixed<Q24_8>(3);
            var f2 = new Fixed<Q24_8>(2);
            Assert.AreEqual("6", f1.Multiply(f2).ToString());
        }
        [TestMethod]
        public void TestOverFlow()
        {
            var f1 = new Fixed<Q24_8>(19);
            var f2 = new Fixed<Q24_8>(13);
            Assert.AreEqual("247", f1.Multiply(f2).ToString());
        }
        [TestMethod]
        public void TestSimpleDiv()
        {
            var f1 = new Fixed<Q24_8>(3);
            var f2 = new Fixed<Q24_8>(2);
            Assert.AreEqual("1,5", f1.Divide(f2).ToString());
        }
        [TestMethod]
        public void TestDivide1()
        {
            var f1 = new Fixed<Q24_8>(248);
            var f2 = new Fixed<Q24_8>(10);
            Assert.AreEqual("24,796875", f1.Divide(f2).ToString());
        }
        [TestMethod]
        public void TestDivide2()
        {
            var f1 = new Fixed<Q24_8>(625);
            var f2 = new Fixed<Q24_8>(1000);
            Assert.AreEqual("0,625", f1.Divide(f2).ToString());
        }
    }
    [TestClass]
    public class TestSubtract
    {
        [TestMethod]
        public void TestQ24_8()
        {
            var f1 = new Fixed<Q24_8>(256);
            var f2 = new Fixed<Q24_8>(-256);
            Assert.AreEqual("512", f1.Subtract(f2).ToString());
        }
        [TestMethod]
        public void TestQ16_16()
        {
            var f1 = new Fixed<Q16_16>(256);
            var f2 = new Fixed<Q16_16>(-256);
            Assert.AreEqual("512", f1.Subtract(f2).ToString());
        }
        [TestMethod]
        public void TestQ8_24()
        {
            var f1 = new Fixed<Q8_24>(256);
            var f2 = new Fixed<Q8_24>(256);
            Assert.AreEqual("0", f1.Subtract(f2).ToString());
        }
    }
    [TestClass]
    public class TestOperatorsEquals
    {
        [TestMethod]
        public void TestAdd()
        {
            System.Random rnd = new System.Random();
            for (int i = 0; i < 5; i++)
            {
                int a = rnd.Next(), b = rnd.Next();
                var f1 = new Fixed<Q8_24>(a);
                var f2 = new Fixed<Q8_24>(b);
                Assert.AreEqual(f1.Add(f2).ToString(), (f1 + f2).ToString());
                var f3 = new Fixed<Q16_16>(a);
                var f4 = new Fixed<Q16_16>(b);
                Assert.AreEqual(f3.Add(f4).ToString(), (f3 + f4).ToString());
                var f5 = new Fixed<Q24_8>(a);
                var f6 = new Fixed<Q24_8>(b);
                Assert.AreEqual(f5.Add(f6).ToString(), (f5 + f6).ToString());
            }
        }
        [TestMethod]
        public void TestSubtract()
        {
            System.Random rnd = new System.Random();
            for (int i = 0; i < 5; i++)
            {
                int a = rnd.Next(), b = rnd.Next();
                var f1 = new Fixed<Q8_24>(a);
                var f2 = new Fixed<Q8_24>(b);
                Assert.AreEqual(f1.Subtract(f2).ToString(), (f1 - f2).ToString());
                var f3 = new Fixed<Q16_16>(a);
                var f4 = new Fixed<Q16_16>(b);
                Assert.AreEqual(f3.Subtract(f4).ToString(), (f3 - f4).ToString());
                var f5 = new Fixed<Q24_8>(a);
                var f6 = new Fixed<Q24_8>(b);
                Assert.AreEqual(f5.Subtract(f6).ToString(), (f5 - f6).ToString());
            }
        }
        [TestMethod]
        public void TestMultiply()
        {
            System.Random rnd = new System.Random();
            for (int i = 0; i < 5; i++)
            {
                int a = rnd.Next(), b = rnd.Next();
                var f1 = new Fixed<Q8_24>(a);
                var f2 = new Fixed<Q8_24>(b);
                Assert.AreEqual(f1.Multiply(f2).ToString(), (f1 * f2).ToString());
                var f3 = new Fixed<Q16_16>(a);
                var f4 = new Fixed<Q16_16>(b);
                Assert.AreEqual(f3.Multiply(f4).ToString(), (f3 * f4).ToString());
                var f5 = new Fixed<Q24_8>(a);
                var f6 = new Fixed<Q24_8>(b);
                Assert.AreEqual(f5.Multiply(f6).ToString(), (f5 * f6).ToString());
            }
        }
        [TestMethod]
        public void TestDivide()
        {
            System.Random rnd = new System.Random();
            for (int i = 0; i < 5; i++)
            {
                int a = rnd.Next(), b = rnd.Next();
                var f1 = new Fixed<Q8_24>(a);
                var f2 = new Fixed<Q8_24>(b);
                Assert.AreEqual(f1.Divide(f2).ToString(), (f1 / f2).ToString());
                var f3 = new Fixed<Q16_16>(a);
                var f4 = new Fixed<Q16_16>(b);
                Assert.AreEqual(f3.Divide(f4).ToString(), (f3 / f4).ToString());
                var f5 = new Fixed<Q24_8>(a);
                var f6 = new Fixed<Q24_8>(b);
                Assert.AreEqual(f5.Divide(f6).ToString(), (f5 / f6).ToString());
            }
        }
    }
}
